
import { ADD_ENCODED,
	 SET_INPUT,
	 SET_QR_CODE,
	 SET_ACCELEROMETER_DATA,
	 TOGGLE_SUBSCRIBED,
	 TOGGLE_SUBSCRIBED_TWO,
	 SET_BALL_POSITION_X,
       	 SET_BALL_POSITION_Y,
	 GENERATE_DOT } from "../constants/action-types";

export function addEncoded(payload) {
  return { type: ADD_ENCODED, payload };
}

export function setInput(payload) {
  return { type: SET_INPUT, payload };
}

export function setQrCode(payload) {
    return { type: SET_QR_CODE, payload };
}

export function setAccelerometerData(payload) {
    return { type: SET_ACCELEROMETER_DATA, payload };
}

export function toggleSubscribed(payload) {
    return { type: TOGGLE_SUBSCRIBED, payload };
}

export function toggleSubscribedTwo(payload) {
    return { type: TOGGLE_SUBSCRIBED_TWO, payload };
}

export function setBallPositionX(payload) {
    return { type: SET_BALL_POSITION_X, payload };
}

export function setBallPositionY(payload) {
    return { type: SET_BALL_POSITION_Y, payload };
}

export function generateDot(payload) {
    return { type: GENERATE_DOT, payload };
}
