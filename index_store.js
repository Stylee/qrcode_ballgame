import store from "./store/index";
import { hitMonster } from "./actions/index";

window.store = store;
window.hitMonster = hitMonster;
