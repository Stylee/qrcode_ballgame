import { Dimensions } from 'react-native';

import { ADD_ENCODED,
	 SET_INPUT,
	 SET_QR_CODE,
	 SET_MOVEMENT,
	 SET_ACCELEROMETER_DATA,
	 TOGGLE_SUBSCRIBED,
	 TOGGLE_SUBSCRIBED2,
	 SET_BALL_POSITION_X,
	 SET_BALL_POSITION_Y,
	 GENERATE_DOT } from "../constants/action-types";


const initialState = {
    app: {
	inputValue: '',
	valueForQRCode: ''
    },
    Encoded: {
    },
    Decoded: {
    },
    BallGame: {
	accelerometerDatas: {
	    x: 0,
	    y: 0,
	    z: 0
	},
	subscribed: 0,
	ballPositionX: 170,
	ballPositionY: 350,
	dotPosition: {
	    x: 50,
	    y: 100
	}
    }
};

function rootReducer(state = initialState, action) {

    if (action.type === ADD_ENCODED) {
    	return {
            ...state,
    	    Encoded: {
    		...state.Encoded,
                section: action.payload
            }
        };
	
	// Object.assign({}, state, {
	//     monster: state.monster.concat(action.payload)
	// });
	// }
    }

    if (action.type === SET_INPUT) {
    	return {
            ...state,
    	    app: {
    		...state.app,
                inputValue: action.payload
            }
        };
    }

    if (action.type === SET_QR_CODE) {
    	return {
            ...state,
    	    app: {
    		...state.app,
                valueForQRCode: action.payload
            },
	    Encoded: {
		...state
		.Encoded,
		[state.app.inputValue]: action.payload
	    }
        };
    }

    if (action.type === SET_ACCELEROMETER_DATA) {
        // console.log("payload", action.payload);
        // console.log("state", state.BallGame.accelerometerDatas.x);
    	return {
            ...state,
    	    BallGame: {
    		...state.BallGame,
                accelerometerDatas: action.payload
            }
        };
    }

    if (action.type === TOGGLE_SUBSCRIBED) {
    	return {
            ...state,
    	    BallGame: {
    		...state.BallGame,
                subscribed: action.payload
            }
        };
    }

    if (action.type === GENERATE_DOT) {

	let xMax = Dimensions.get('window').width - 80;
	let xMin = 0;

	let  yMax = Dimensions.get('window').height - 120;
	let  yMin = 0;

	return {
            ...state,
	    BallGame: {
		...state.BallGame,
                dotPosition: {
		    x: Math.random() * (xMax - xMin) + xMin,
		    y: Math.random() * (yMax - yMin) + yMin
		}
            }
	};
    }

    if (action.type === SET_BALL_POSITION_X) {
	    let oldPosition = state.BallGame.ballPositionX;
	    let position = oldPosition;
	    
            if (action.payload > 0) {
		if (oldPosition > 0) {
	            position = oldPosition - action.payload *100;
		}
            }

            if (action.payload < 0) {
		if (oldPosition < Dimensions.get('window').width - 50) {
	            position = oldPosition - action.payload *100;
		}
            }
	    
    	    return {
		...state,
    		BallGame: {
    		    ...state.BallGame,
                    ballPositionX: position
		}
            };
	}

	if (action.type === SET_BALL_POSITION_Y) {

	    let oldPosition = state.BallGame.ballPositionY;
	    let position = oldPosition;

	    if (action.payload < 0) {
		if (oldPosition > 0) {
	            position = oldPosition + action.payload *100;
		}
            }

            if (action.payload > 0) {
		if (oldPosition < Dimensions.get('window').height - 100) {
	            position = oldPosition + action.payload *100;
		}
            }
	    
    	    return {
		...state,
    		BallGame: {
    		    ...state.BallGame,
                    ballPositionY: position
		}
            };
            
	}


	return state;
};

export default rootReducer;
