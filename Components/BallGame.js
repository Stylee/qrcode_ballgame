 // https://paste.ubuntu.com/p/7FDPJdgqvH/
 
'use strict';

import React from 'react';
import { Button, View, StyleSheet, Text, TouchableOpacity, Image, Dimensions } from 'react-native';

import * as Permissions from 'expo-permissions';

import { Accelerometer } from 'expo-sensors';

import { connect } from 'react-redux';

import { setAccelerometerData, toggleSubscribed, toggleSubscribedTwo, setBallPositionX, setBallPositionY, generateDot } from "../actions/index";

const mapDispatchToProps = (dispatch) => {
    return {
        setAccelerometerData: (payload) => dispatch(setAccelerometerData(payload)),
        toggleSubscribed: (payload) => dispatch(toggleSubscribed(payload)),
        toggleSubscribedTwo: (payload) => dispatch(toggleSubscribedTwo(payload)),
        setBallPositionX: (payload) => dispatch(setBallPositionX(payload)),
        setBallPositionY: (payload) => dispatch(setBallPositionY(payload)),
        generateDot: (payload) => dispatch(generateDot(payload))
    };
};

const mapStateToProps = (state, ownProps) => {
    
    return {
        props: ownProps,
	state: state
    };
};

const BallGameScreenConnect = (props) => {

    // console.log(Accelerometer.isAvailableAsync());
    // console.log("BALLGAME", props.state.BallGame.accelerometerData) ;
    // console.log("PROPZS", props);

    let subscription;

    const _toggle = () => {
	if (props.state.BallGame.subscribed == 1) {
	    _unsubscribe();

	} else {
	    _subscribe();
	}
    };

    const _slow = () => {
	Accelerometer.setUpdateInterval(1000);
    };

    const _fast = () => {
	Accelerometer.setUpdateInterval(16);
    };

    const _subscribe = () => {
        props.toggleSubscribed(1);
        // props.generateDot();           Turns out I don't know anymore how to use actions past 21:30 :/
        // props.toggleSubscribedTwo(1);   Time to bed...

	subscription = Accelerometer.addListener(accelerometerData => {
	    props.setAccelerometerData( accelerometerData );
	    // console.log('positionx', props.state.BallGame.accelerometerDatas.x); NO IDEA WHY I DON'T have access to an updated state here !?!?!?
            props.setBallPositionX(accelerometerData.x);
            props.setBallPositionY(accelerometerData.y);

	});
    };

    const _unsubscribe = () => {
	// subscription && subscription.remove(); // this doesn't works
        Accelerometer.removeAllListeners(); // this does
        props.toggleSubscribed(0);
    };

    // Accelerometer.addListener(item => {
    // 	props.setMovement(item.x *-100);
    // });

    function round(n) {
        if (!n) {
            return 0;
        }

        return Math.floor(n * 100) / 100;
    }


    // console.log('x', props.state.BallGame.accelerometerData.x);
    // console.log('y', props.state.BallGame.accelerometerData.y);
    // console.log('z', props.state.BallGame.accelerometerData.z);

    let { x, y, z } = props.state.BallGame.accelerometerDatas;
    

    return (
        <View style={{ flex: 1, backgroundColor: "white"}}>
	  <Text>
	    Ball Game
	  </Text>

          <Image source={require("../assets/gnu.png")}
                 style={{width: 70,
                         height: 50,
                         position: 'absolute',
                         /* left:  */
                         /* top: round(props.state.BallGame.accelerometerData.y) */
                         /* left: round(props.state.BallGame.ballPositionX *-100) */
                         left: props.state.BallGame.dotPosition.x,
                         top:  props.state.BallGame.dotPosition.y
                        }}/>
          
          <Image source={require("../assets/bille.png")}
                 style={{width: 70,
                         height: 50,
                         position: 'absolute',
                         /* left:  */
                         /* top: round(props.state.BallGame.accelerometerData.y) */
                         /* left: round(props.state.BallGame.ballPositionX *-100) */
                         left: props.state.BallGame.ballPositionX,
                         top:  props.state.BallGame.ballPositionY
                        }}/>

          
	  <View style={styles.sensor}>
            <Text>Accelerometer:</Text>
            <Text>
              x: {round(x)} y: {round(y)} z: {round(z)}
            </Text>
            <Text>BallPosition:</Text>
            <Text>
              x: {round(props.state.BallGame.ballPositionX)}    y: {round(props.state.BallGame.ballPositionY)}    z: {round(z)}

            </Text>
            <View style={styles.buttonContainer}>
              <TouchableOpacity onPress={_toggle} style={styles.button}>
                <Text>Toggle</Text>
              </TouchableOpacity>
              <TouchableOpacity onPress={_slow} style={[styles.button, styles.middleButton]}>
                <Text>Slow</Text>
              </TouchableOpacity>
              <TouchableOpacity onPress={_fast} style={styles.button}>
                <Text>Fast</Text>
              </TouchableOpacity>
            </View>
	  </View>

	  
        </View>
    );
};

const styles = StyleSheet.create({
    buttonContainer: {
	flexDirection: 'row',
	alignItems: 'stretch',
	marginTop: 15,
    },
    button: {
	flex: 1,
	justifyContent: 'center',
	alignItems: 'center',
	backgroundColor: '#eee',
        height: 40
    },
    middleButton: {
	borderLeftWidth: 1,
	borderRightWidth: 1,
	borderColor: '#ccc',
    },
    sensor: {
	marginTop: 45,
	paddingHorizontal: 10,
    },
});

const BallGameScreen =
    connect(mapStateToProps, mapDispatchToProps)(BallGameScreenConnect);


export default BallGameScreen;
