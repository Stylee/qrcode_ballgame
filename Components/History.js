import React from 'react';
import { Button, StyleSheet, Text, View } from 'react-native';

import { connect } from 'react-redux';

const mapStateToProps = (state, ownProps) => {
    return {
        props: ownProps,
	state: state,
	Encoded: state.Encoded
    };
};


const HistoryScreenConnect = (props) => {
 console.log("STATE", props.Encoded);

    const displayHistory = () => {
        const obj = props.Encoded;
	return Object.keys(obj).map(key => 
            <Text value={key}>{obj[key]}</Text>
        );
    };
    
    
    return(
        <View style={
            { flex: 1, alignItems: "center", justifyContent: "center" }
        }>
        <Text>History Screen</Text>

	{displayHistory()}
	</View>
    );
};

const styles = StyleSheet.create({
    container: {
	flex: 1,
	backgroundColor: '#fff',
	alignItems: 'center',
	justifyContent: 'center',
    },
});

const HistoryScreen =
    connect(mapStateToProps)(HistoryScreenConnect);

export default HistoryScreen;
